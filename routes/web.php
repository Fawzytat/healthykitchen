<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->middleware('auth');

Route::resource('feedbacks', 'feedbacksController');
Route::resource('meals', 'MealController');
Route::resource('cities', 'CityController');

Route::get('/admin', 'PagesController@adminhome')->middleware('auth', 'adminApproved');
Route::get('/business', 'PagesController@business')->middleware('auth', 'adminApproved');
Route::get('/policyEdit', 'PagesController@policyEdit')->middleware('auth', 'adminApproved');
Route::get('/orders', 'OrdersController@view')->middleware('auth', 'adminApproved');
//Route::get('/cities', 'PagesController@citiesupdate')->middleware('auth', 'adminApproved');




Route::post('/businessUpdate', 'UpdateInfoController@update');
Route::post('/updateTerms', 'UpdateInfoController@updateTerms');
Route::post('/updatePolicy', 'UpdateInfoController@updatePolicy');
Route::post('/aboutUpdate', 'UpdateInfoController@aboutUpdate');



Route::post('/addToCart','CartController@addToCart');
Route::get('/getCart','CartController@getCart');
Route::post('/updateCart','CartController@updateCart');


Route::post('/placeOrder', 'OrdersController@place')->middleware('auth');
Route::get('/checkout','CartController@checkout')->middleware('auth');

Route::get('/myorders','PagesController@myorders')->middleware('auth');
Route::get('/Myprofile','PagesController@myprofile')->middleware('auth');

Route::post('/updateProfile','PagesController@updateProfile')->middleware('auth');


Route::get('/privacypolicy','PagesController@privacypolicy');
Route::get('/terms','PagesController@terms');




Route::get('/emptyCart', 'CartController@emptyCart');
//Route::post('/makeFeatured' , 'MealController@makeFeatured' );
Route::post('makeFeatured',[
    'as' => 'makeFeatured',
    'uses' => 'popularController@makeFeatured'
]);

Route::post('makeunFeatured',[
    'as' => 'makeunFeatured',
    'uses' => 'popularController@makeunFeatured'
]);

Route::get('/{unlocatedUrl}', function ($hamada) {
    return view('errors.404');
});


Auth::routes();

//Route::get('/home', 'HomeController@index');
