<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Order;

class notifyAdmin extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
     protected $items;

     public function __construct(Order $order)
     {
         $this->order = $order ;
     }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('New order placed')
                    ->line('New order placed , Order ID #'.$this->order->id)
                    ->action('Check all orders', url('/orders'))
                    ->line('You got a new order with total of' . $this->order->total. '$  ' )
                    ->line('Order details   ' . $this->order->fullOrder )
                    ->line('user card details' )
                    ->line('Card no | '. $this->order->cred_no )
                    ->line('Expiry date | '. $this->order->exp_date)
                    ->line('CVV | '.$this->order->cvv );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
