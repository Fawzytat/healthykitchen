<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
  protected $fillable = [
      'phone', 'mail', 'name', 'subject', 'message'
  ];

  protected $table = 'feedbacks';
}
