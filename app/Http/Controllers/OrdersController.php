<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meal;
use Log;
use Cart;
use Auth;
use App\Order;
use App\User;
use App\Notifications\orderPlaced;
use App\Notifications\notifyAdmin;

class OrdersController extends Controller
{
    public function view()
    {
      $orders = Order::all();

      return view('admin.orders')->withOrders($orders);
    }


    public function place(Request $request)
    {
      Log::debug($request);
      //validating the request
      $this->validate($request, [
       'cvv' => 'required|digits:3',
       'cred_no' => 'required|digits:16',
       'exp_mon' => 'required|digits:2',
       'exp_year' => 'required|digits:2',
      ]);

      //sending Email
      $user = Auth::user();
      $user_id = Auth::user()->id;
      $order_total = Cart::subtotal();
      $items =  Cart::content();
      $admins = User::where('type','admin')->get();


      // Order details


      // creating the orders json
      $orders = array();
      foreach ($items as $item) {
        $order_name = $item->name ;
        $order_qty = $item->qty;
        $order = ($order_qty . ' orders of ' . $order_name);
        array_push($orders, $order);
      }

      $orders_json = json_encode($orders);
      Log::debug($orders_json);

      //Storing order in DB
      $newOrder = new Order;
      $newOrder->user_id = $user_id;
      $newOrder->city = $request->city;
      $newOrder->notes = $request->notes;
      $newOrder->address = $request->address;
      $newOrder->cred_no = $request->cred_no;
      $newOrder->exp_date = $request->exp_mon . ' / ' . $request->exp_year;
      $newOrder->cvv = $request->cvv;
      $newOrder->total = $order_total;
      $newOrder->fullOrder = $orders_json;
      $newOrder->delivery_date = $request->delivery_date;
      $newOrder->save();


      $user->notify(new orderPlaced($newOrder));
      foreach ($admins as $admin) {
        $admin->notify(new notifyAdmin($newOrder));
      }

      Cart::destroy();

      $Response   = array(
             'success' => '1',
             'message' => 'You have to confirm your email address first '
         );

      return $Response;
    //  return redirect('home')->with('status' , ' Thank you , Order added succefully');

    }
}
