<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meal;
use App\Category;

class MealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $meals = Meal::all();
      $categories = Category::all();
      return view('admin.meals')->withMeals($meals)->withCategories($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $meal = new Meal;
        $meal->name = $request->name;
        $meal->mid = $request->mid;
        $meal->category_id = $request->category;
        $meal->price = $request->price;
        $meal->ingredients = $request->description;
        $meal->save();

        return back()->with('status' , 'Meal added succefully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $meal = Meal::where('id' , $request->editMealId)->first();
        $meal->name = $request->name;
        $meal->mid = $request->mid;
        $meal->category_id = $request->category;
        $meal->price = $request->price;
        $meal->ingredients = $request->description;
        $meal->save();

        return back()->with('status' , 'Meal Updated succefully');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $meal = Meal::where('id' , $request->deletedMealId);
        $meal->delete();
        return back()->with('status' , 'Meal Deleted succefully');
    }
}
