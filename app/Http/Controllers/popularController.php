<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meal;
use App\Category;
use Log;


class popularController extends Controller
{
  public function makeFeatured(Request $request)
  {
      $meal = Meal::where("id" ,$request->addToPopMeal)->first();

       $file = $request->file('featuredImage');
       $ext = $file->extension();
      //  $path = $file->storeAs('meals/'. $request->addToPopMeal , "image.{$ext}");
      //  $meal->picture = $path;

      $name = $meal->id . '-' . $file->getClientOriginalName();
      $meal->picture = $name;
      $file->move(public_path() . '/eu_assets/images/pop_meals/', $name);

       $meal->popular = "yes";
       $meal->save();





       return back()->with('status' , 'Meal added to Most popular succefully');
  }

  public function makeunFeatured(Request $request)
  {
    $meal = Meal::where("id" ,$request->remFromPopMeal)->first();
    $meal->popular = "no";
    $meal->save();
    return back()->with('status' , 'Meal is no longer a most popular meal ');

  }
}
