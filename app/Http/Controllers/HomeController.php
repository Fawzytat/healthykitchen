<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Businessinfo;
use App\Category;
use App\Meal;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function welcome(){
      $info = Businessinfo::where("id" ,1)->first();
      $categories = Category::get();
      $foods = Meal::get();
      return view('welcome')->withInfo($info)
                            ->withCategories($categories)
                            ->withFoods($foods);

    }
    public function index()
    {
        $foods = Meal::get();
        $categories = Category::get();
        if( Auth::user() && Auth::user()->type =="admin"){
          return redirect('/');
        }
        return view('welcome')->withCategories($categories)->withFoods($foods);
    }
}
