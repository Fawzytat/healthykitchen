<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Businessinfo;
use App\Order;
use App\City;
use App\User;
use Auth;

class PagesController extends Controller
{
    public function adminhome()
    {
      return view('admin.index');
    }

    public function business()
    {
      $info = Businessinfo::where("id" ,1)->first();
      return view('admin.business')->withInfo($info);
    }

    public function citiesupdate()
    {
      return view('admin.cities');
    }

    public function policyEdit()
    {
      $info = Businessinfo::where("id" ,1)->first();
      return view('admin.policy')->withInfo($info);
    }

    public function myorders()
    {
      $user_id =  Auth::user()->id;
      $orders = Order::where('user_id', $user_id )->get();
      return view('myorders')->withOrders($orders);
    }

    public function myprofile()
    {
      $user =  Auth::user();
      return view('myprofile');
    }

    public function updateProfile(Request $request)
    {
      $user =  Auth::user();
      User::where('id', $user->id)
      ->update(['phone' => $request->phone,
                'address' => $request->address,
                'name' => $request->name,
                ]);

    return back()->with('status' , ' personal info updated !');
    }

    public function privacypolicy()
    {
      return view('policy');
    }

    public function terms()
    {
      return view('terms');
    }
}
