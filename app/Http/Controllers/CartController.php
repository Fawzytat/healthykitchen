<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meal;
use Session;
use Log;
use Cart;

class CartController extends Controller
{

     public function __construct()
     {
         $this->middleware('auth');
     }


    public function addToCart(Request $Request)
    {

      $meal = Meal::where('id' , $Request->item_id)->first();
      Log::debug($meal);

      if (!$meal) {
            return view('errors.404');
        }

      Cart::add($meal->id, $meal->name, 1, $meal->price);

    }

    public function updateCart(Request $Request)
    {
      Cart::update($Request->rowId, $Request->quantity);

    }

    public function getCart()
    {

    $items =  Cart::content();

    if (!$items) {
          return view('errors.404');
      }
    return View('cart')->withItems($items);
    }

    public function checkout()
    {
      $items =  Cart::content();

      if (!$items) {
            return view('errors.404');
        }
        
      return View('checkout')->withItems($items);
    }

    public function emptyCart()
    {
      Cart::destroy();
    }
}
