<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Businessinfo;
use Log;


class UpdateInfoController extends Controller
{
    public function update(Request $request)
    {
          Businessinfo::where('id', 1)
          ->update(['phone' => $request->phone,
                    'email' => $request->email,
                    'kitchen_rules' => $request->kitchen_rules,
                    ]);

      return redirect('business')->with('status' , ' business info updated !');
    }

    public function aboutUpdate(Request $request)
    {
      Log::debug($request);
      Businessinfo::where('id', 1)
      ->update(['about' => $request->aboutus,
                ]);
    return redirect('business')->with('status' , ' About section updated !');
    }


    public function updatePolicy(Request $request)
    {
      Businessinfo::where('id', 1)
      ->update(['privacy_title1' => $request->privacy_title1,
                'privacy_title2' => $request->privacy_title2,
                'privacy_title3' => $request->privacy_title3,
                'privacy_title4' => $request->privacy_title4,
                'privacy_text1' => $request->privacy_text1,
                'privacy_text2' => $request->privacy_text2,
                'privacy_text3' => $request->privacy_text3,
                'privacy_text4' => $request->privacy_text4,

                ]);

      return redirect('policyEdit')->with('status' , ' policy info updated !');
    }

    public function updateTerms(Request $request)
    {
      Businessinfo::where('id', 1)
      ->update(['terms' => $request->ourterms,


                ]);

      return redirect('policyEdit')->with('status' , ' Terms info updated !');
    }


}
