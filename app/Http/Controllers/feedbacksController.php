<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;
use App\User;
use App\Notifications\messageAdmin;

class feedbacksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feedbacks = Feedback::all();

        if (!$feedbacks) {
              return view('errors.404');
          }
        return view('admin.feedbacks')->withFeedbacks($feedbacks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $admins = User::where('type','admin')->get();

        if (!$admins) {
              return view('errors.404');
          }
        $feedback = new Feedback;
        $feedback->name = $request->name;
        $feedback->mail = $request->email;
        $feedback->phone = $request->phone;
        $feedback->message = $request->message;
        $feedback->subject = $request->subject;
        $feedback->save();
        foreach ($admins as $admin ) {
          $admin->notify(new messageAdmin($feedback));
        }


        return redirect('/')->with('status', 'Thank you , We got your message!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
