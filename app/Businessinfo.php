<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Businessinfo extends Model
{
    protected $fillable = [
        'phone', 'email', 'kitchen_rules', 'about', 'privacy_policy', 'terms'
    ];

    protected $table = 'businessinfo';
}
