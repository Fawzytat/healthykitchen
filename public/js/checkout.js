

$(document).ready(function(){
        var checkoutForm = $("#checkoutForm");
        checkoutForm.submit(function(e){
            e.preventDefault();
            var formData = checkoutForm.serialize();
            $( '#checkout-errors-cred_no' ).html( "" );
            $( '#checkout-errors-exp_mon' ).html( "" );
            $( '#checkout-errors-exp_year' ).html( "" );
            $( '#checkout-errors-cvv' ).html( "" );

            $("#checkout-cred_no").removeClass("has-error");
            $("#checkout-exp_mon").removeClass("has-error");
            $("#checkout-exp_year").removeClass("has-error");
            $("#checkout-cvv").removeClass("has-error");


            $.ajax({
                url:'/placeOrder',
                type:'POST',
                data:formData,
                success:function(data){

                  //window.location.href = '/login';
                  if (data.success == 1)
                  {
                      $('#OrderConfirm').removeClass("hidden");
                      setTimeout(function() {
                            window.location.href = "/";
                          }, 10000);
                  }
                },
                error: function (data) {
                    console.log(data.responseText);
                    var obj = jQuery.parseJSON( data.responseText );
                   if(obj.cred_no){
                        $("#checkout-cred_no").addClass("has-error");
                        $( '#checkout-errors-cred_no' ).html( obj.cred_no );
                    }
                    if(obj.exp_mon){
                         $("#checkout-exp_mon").addClass("has-error");
                         $( '#checkout-errors-exp_mon' ).html( obj.exp_mon );
                     }

                     if(obj.exp_year){
                          $("#checkout-exp_year").addClass("has-error");
                          $( '#checkout-errors-exp_year' ).html( obj.exp_year );
                      }

                     if(obj.cvv){
                          $("#checkout-cvv").addClass("has-error");
                          $( '#checkout-errors-cvv' ).html( obj.cvv );
                      }

                }
            });
        });
});
