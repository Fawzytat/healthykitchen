
    $('.search-icon').click(function () {
        $('.search-bar').show(300);
    });


    $(".addItem").click(function() {
        var id = $(this).attr('id'); // $(this) refers to button that was clicked


                $.ajax({
            url: '/addToCart',
            type: 'post',
            data: {
                _token: $('input[name="_token"]').val(),
                item_id: id,

            },
            success: function (data) {

            },
            error: function (data) {
             window.location.href = "/login";
            }
                });

        $(this).hide();
        $(this).next().removeClass('hidden');

    });
