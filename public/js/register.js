

$(document).ready(function(){
        var registerForm = $("#registerForm");
        registerForm.submit(function(e){
            e.preventDefault();
            var formData = registerForm.serialize();
            $( '#register-errors-name' ).html( "" );
            $( '#register-errors-lname' ).html( "" );
            $( '#register-errors-email' ).html( "" );
            $( '#register-errors-phone' ).html( "" );
            $( '#register-errors-address' ).html( "" );
            $( '#register-errors-password' ).html( "" );
            $("#register-name").removeClass("has-error");
            $("#register-lname").removeClass("has-error");
            $("#register-email").removeClass("has-error");
            $("#register-phone").removeClass("has-error");
            $("#register-address").removeClass("has-error");
            $("#register-password").removeClass("has-error");

            $.ajax({
                url:'/register',
                type:'POST',
                data:formData,
                success:function(data){
                    //window.location.href = '/login';
                    if (data.success == 1)
                    {

                        $('#confirmMessage').removeClass("hidden");
                        setTimeout(function() {
                              window.location.href = "/login";
                            }, 7000);
                    }

                },
                error: function (data) {
                    console.log(data.responseText);
                    var obj = jQuery.parseJSON( data.responseText );
                   if(obj.name){
                        $("#register-name").addClass("has-error");
                        $( '#register-errors-name' ).html( obj.name );
                    }
                    if(obj.lname){
                         $("#register-lname").addClass("has-error");
                         $( '#register-errors-lname' ).html( obj.lname );
                     }
                     if(obj.phone){
                          $("#register-phone").addClass("has-error");
                          $( '#register-errors-phone' ).html( obj.phone );
                      }
                      if(obj.address){
                           $("#register-address").addClass("has-error");
                           $( '#register-errors-address' ).html( obj.address );
                       }
                    if(obj.email){
                        $("#register-email").addClass("has-error");
                        $( '#register-errors-email' ).html( obj.email );
                    }
                    if(obj.password){
                        $("#register-password").addClass("has-error");
                        $( '#register-errors-password' ).html( obj.password );
                    }
                }
            });
        });
});
