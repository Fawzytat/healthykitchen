$(".pluss").click(function() {
    var price = $(this).prev().attr('price');
    var rowId = $(this).prev().attr('id');
    var currentQuantity =  parseInt($(this).prev().val());
    var quantity = currentQuantity + 1;
    $(this).prev().val(quantity);
    // updating the cart with the new quantity
    $.ajax({
              url: '/updateCart',
              type: 'post',
              data: {
                  _token: $('input[name="_token"]').val(),
                  rowId: rowId,
                  quantity: quantity

              },
              success: function (data) {
              console.log('Cart updated !');
              },
              error: function (data) {
              console.log('error happend  !');
              }
                    });

    // changing the row total
    var total = price*quantity;
    $(this).closest('td').next('td').text(total);

    // change the all total
    var currentAllTotal = parseInt($('#alltotal').text());
    var allTotal = currentAllTotal + parseInt(price);
    $('#alltotal').text(allTotal);
    //alert(quantity);

});

  /////////////////// minuus button //////////////////////


$(".minuus").click(function() {
    var price = $(this).next().attr('price');
    var rowId = $(this).next().attr('id');
    var currentQuantity =  parseInt($(this).next().val());
    if(currentQuantity != 1)
    {
      quantity = currentQuantity - 1;
      $(this).next().val(quantity);

      // updating the cart with the new quantity
      $.ajax({
                url: '/updateCart',
                type: 'post',
                data: {
                    _token: $('input[name="_token"]').val(),
                    rowId: rowId,
                    quantity: quantity

                },
                success: function (data) {
                console.log('Cart updated !');
                },
                error: function (data) {
                console.log('error happend  !');
                }
                      });

      // changing the row total
      var total = price*quantity;
      $(this).closest('td').next('td').text(total);

      // change the all total
      var currentAllTotal = parseInt($('#alltotal').text());
      var allTotal = currentAllTotal - parseInt(price);
      $('#alltotal').text(allTotal);
      //alert(allTotal);
    }



});
