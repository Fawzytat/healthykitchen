@extends('layouts.hk_layout')

@section('content')





        <!--  Page Navigation -->
                  <div class="header-page">
                      <div class="container center">
                          <div class="row">
                              <h1>login / register</h1>
                              <img class="img-responsive" src="eu_assets/images/title-shape.png" alt="">
                              <p>Glad to serve the best for you</p>
                          </div>
                      </div>
                  </div>
        <!-- End Page Navigation ---->
        <!-- new login-->




        <div class="login">
            <div class="container center">
              
                <div class="row">

                    <div class="tabs clearfix">

                        <ul class="tab-nav clearfix">
                            <li><a href="#login">login</a></li>
                            <li><a href="#register">register</a></li>
                        </ul>



                        <div class="tab-container">
                            <div class="tab-content clearfix" id="login">
                              <div class="contact-form">
                                  <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}
                                          <div class="row">
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <input id="email" type="email" class="sm-form-control daterange1" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>

                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                          </div>

                                          <div class="row">

                                              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">


                                                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                      <input id="password" type="password" placeholder="Password" class="sm-form-control daterange1" name="password" required>

                                                      @if ($errors->has('password'))
                                                          <span class="help-block">
                                                              <strong>{{ $errors->first('password') }}</strong>
                                                          </span>
                                                      @endif
                                                  </div>
                                              </div>

                                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                  <a href="{{ route('password.request') }}">Forgot Password ?</a>
                                              </div>

                                          </div>

                                          <div class="row">
                                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                  <button type="submit" class="button button-mini">login</button>
                                              </div>
                                          </div>
                                      </form>
                                  </div>
                              </div>
                            </div>
                            <div class="tab-content clearfix" id="register">
                              <div class="contact-form">
                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
                                    <form class="form-horizontal" role="form" id="registerForm" method="POST" action="">
                                        {{ csrf_field() }}

                                          <div class="row">

                                              <div class="form-group" id="register-name">
                                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                      <input id="name" type="text" placeholder="First Name" class="sm-form-control daterange1" name="name" value="{{ old('name') }}" required autofocus>

                                                          <span class="help-block"><strong id="register-errors-name"></strong></span>

                                                  </div>

                                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                      <input id="lname" type="text" placeholder="Last Name" class="sm-form-control daterange1" name="lname" value="{{ old('lname') }}" required autofocus>
                                                      <span class="help-block"><strong id="register-errors-lname"></strong></span>
                                                  </div>
                                              </div>


                                          </div>

                                          <div class="row">
                                            <div class="form-group" id="register-email">
                                                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <input id="email" placeholder="Email" type="email" class="sm-form-control daterange1" name="email" value="{{ old('email') }}" required>


                                                        <span class="help-block">
                                                            <strong id="register-errors-email"></strong>
                                                        </span>

                                                  </div>
                                              </div>
                                          </div>

                                          <div class="row">

                                              <div class="form-group" id="register-password">
                                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                      <input id="password" type="password" class="sm-form-control daterange1" placeholder="Password" name="password" required>
                                                          <span class="help-block">
                                                              <strong id="register-errors-password"></strong>
                                                          </span>

                                                  </div>



                                                  <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                      <input id="password-confirm" type="password" class="sm-form-control daterange1" placeholder="Re-Enter Password" name="password_confirmation" required>
                                                      <span clas"help-block"><strong id="form-errors-password-confirm"></strong></span>
                                                  </div>
                                              </div>

                                          </div>

                                          <div class="row">
                                            <div class="form-group" id="register-address">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <input id="address" type="text" class="sm-form-control daterange1" name="address" required
                                                               placeholder="Address">
                                                                   <span class="help-block">
                                                                       <strong id="form-errors-address"></strong>
                                                                   </span>
                                                    </div>
                                              </div>
                                          </div>

                                          <div class="row">
                                            <div class="form-group" id="register-phone">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <input id="phone" type="text" class="sm-form-control daterange1" name="phone" required
                                                               placeholder="Phone">
                                                        <span class="help-block"><strong id="register-errors-phone"></strong></span>
                                                    </div>
                                              </div>
                                          </div>

                                          <div class="row">
                                              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                  <button type="submit" class="button button-mini">register</button>
                                              </div>
                                          </div>

                                          <div class="row">
                                            <div class="alert alert-success hidden" id="confirmMessage" >
                                              <h5> One last step , Please Check your Email to confirm your email address </h5>
                                            </div>
                                          </div>

                                      </form>
                                  </div>
                              </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!---- End Login/Register ----->



        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/register.js"></script>
@endsection
