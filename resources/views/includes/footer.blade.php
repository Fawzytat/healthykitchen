
    <!--  Start Footer -->
    <div class="footer">
        <div class="container center">
            <div class="row">
                <p>We accept all major credit cards</p>
                <div class="img-center">
                    <img class="img-responsive" src="eu_assets/images/visacard.png" alt="">
                    <img class="img-responsive" src="eu_assets/images/mastercard.png" alt="">
                    <img class="img-responsive" src="eu_assets/images/americanexpress.png" alt="">
                </div>

                <div class="terms">
                    <a href="/terms">Terms & Conditions</a>
                    /
                    <a href="privacypolicy">Privacy Policy</a>
                </div>
            </div>
        </div>
    </div><!---- End Footer ----->


    <!--  Start CopyRights -->
    <div class="copyrights">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                    <p>&copy 2017 - healthy kitchen all rights reserved. designed and developed by <a
                            href="http://road9media.com/">road9 media</a></p>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 security">
                    <img class="img-responsive" src="eu_assets/images/comodo.png" alt="">
                </div>
            </div>
        </div>
    </div><!-- End CopyRights ----->

</div><!-- #wrapper end -->


<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="eu_assets/js/jquery.js"></script>
<script type="text/javascript" src="eu_assets/js/plugins.js"></script>





<script type="text/javascript" src="js/welcome.js"></script>
<script type="text/javascript" src="js/cart.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="eu_assets/js/functions.js"></script>

</body>
</html>
