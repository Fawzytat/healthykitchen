<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content="SemiColonWeb"/>

    <link rel="icon" href="eu_assets/images/favicon.png" type="image/x-icon"/>

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/bootstrap.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/style.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/dark.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/font-icons.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/animate.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/magnific-popup.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/healthy.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/responsive.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>


    <link rel="stylesheet" href="css/main.css" type="text/css"/>
    <!-- Document Title
    ============================================= -->
    <title>Healthy Kitchen - Home</title>

</head>
