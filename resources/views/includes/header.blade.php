<?php
use App\Businessinfo;
$info = Businessinfo::where("id" ,1)->first();

?>
<!--  Nav Bar --------------------------->
<header id="header" class="full-header">
    <div id="header-wrap">

        <div class="container clearfix">
            <div class="top-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 hidden-xs">
                            <div class="top-left">
                              <p>
                                 <a href="tel:{{ str_replace(".", "", $info->phone) }}"><strong>Call:  </strong>{{ $info->phone }}  </a>
                              </p>
                              <p>
                                  <a href="mailto:{{ $info->email }}"> <strong>  Email:</strong>{{ $info->email }}</a>
                              </p>

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="top-right">
                              <!-- Login button -->
                              @if (Auth::guest())
                                  <a href="{{ route('login') }}" class="button button-mini hidden-xs">Login</a>
                              @else
                                 <!-- <ul style="list-style: none;" > -->
                                  <li class="dropdown">
                                      <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                          {{ Auth::user()->name }} <span class="caret"></span>
                                      </a> -->

                                      <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">  {{ Auth::user()->name }}  <i class="icon-user"></i><i class="icon-angle-down"></i></a>

                                      <ul class="dropdown-menu" role="menu">
                                          <li>
                                            <a href="/Myprofile">Profile setting</a>
                                          </li>
                                          <li>
                                            <a href="/myorders">My orders</a>
                                          </li>
                                          <li>
                                              <a href="{{ route('logout') }}"
                                                  onclick="event.preventDefault();
                                                           document.getElementById('logout-form').submit();">
                                                  Logout
                                              </a>

                                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                  {{ csrf_field() }}
                                              </form>
                                          </li>

                                      </ul>
                                  </li>
                                   <!-- </ul> -->
                              @endif
                              <!-- End of login button  -->

                                <div class="cart">
                                    <a class="hidden-xs" href="#"><i class="icon-line-search search-icon"></i></a>
                                    <a href="/getCart"><img src="eu_assets/images/cart.png" alt=""></a>
                                    <a id="primary-menu-trigger" class="hidden-sm hidden-md hidden-lg" href=""><i
                                            class="icon-line-menu"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mid-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 hidden-xs">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 data">
                                    <div class="why-us">
                                        <img class="img-responsive" src="eu_assets/images/icon-1.png" alt="">
                                        <p>Fresh & Delicious</p>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 data">
                                    <div class="why-us">
                                        <img class="img-responsive" src="eu_assets/images/icon-2.png" alt="">
                                        <p>Fast Delivery</p>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 data">
                                    <div class="why-us">
                                        <img class="img-responsive" src="eu_assets/images/icon-3.png" alt="">
                                        <p>Event Catering</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="logo">
                                <img class="img-responsive" src="eu_assets/images/logo.png" alt="">
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 hidden-xs">
                            <div class="delivery">
                                <h3>{{ $info->kitchen_rules }}</h3>
                                <!-- <h3>large orders please call in advance</h3> -->
                            </div>
                        </div>
                    </div>


                    <div id="search-bar" class="search-bar hidden-xs">
                        <form action="">
                            <input type="text" class="sm-form-control daterange1" placeholder="Search">
                            <a id="js-submit-form" href=""><i class="icon-line-search"></i></a>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <div class="nav-bar">
            <div class="container center clearfix">

                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu">
                    <ul>
                        <div class="top-left hidden-sm hidden-md hidden-lg">
                            <p><strong>Call:</strong> {{ $info->phone }}</p>
                            <p><strong>Email:</strong> {{ $info->email }}</p>
                        </div>

                        <a href="#" class="button button-mini hidden-sm hidden-md hidden-lg">Login</a>

                        <div class="delivery hidden-sm hidden-md hidden-lg">
                            <h3>{{ $info->kitchen_rules }}</h3>
                            <!-- <h3>large orders please call in advance</h3> -->
                        </div>

                        <div class="nav-center">
                            <li>
                                <a href="/">
                                    <div>Home</div>
                                </a>
                            </li>

                            <li>
                                <a href="/#menusec" data-scrollto=".menu" data-offset="0">
                                    <div>menu</div>
                                </a>
                            </li>

                            <li>
                                <a href="/#contactsec" data-scrollto=".contact-us" data-offset="0">
                                    <div>contact</div>
                                </a>
                            </li>
                        </div>

                        <div id="search-mob" class="search-mob hidden-sm hidden-md hidden-lg">
                            <form action="">
                                <input type="text" class="sm-form-control daterange1" placeholder="Search">
                                <a id="js-submit-mob" href=""><i class="icon-line-search"></i></a>
                            </form>
                        </div>

                    </ul>
                </nav>

            </div>
        </div>
    </div>
</header><!-- End Nav Bar ---->
