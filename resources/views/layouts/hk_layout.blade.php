@include('includes.head')
<body class="stretched">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">


  @include('includes.header')

  @if (session('status'))
  <div id="verticlaaaal">
      <div class="alert alert-success" id="partialsFlash">
          {{ session('status') }}
      </div>
  </div>
  @endif



      @yield('content')
      <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-25127970-25', 'auto');
          ga('send', 'pageview');

        </script>

@include('includes.footer')
