
@extends('layouts.hk_layout')

@section('content')





    <!-------------------------- Page Navigation --------------------------->
    <div class="header-page">
        <div class="container center">
            <div class="row">
                <h1>Personal profile</h1>
                <img class="img-responsive" src="eu_assets/images/title-shape.png" alt="">
                <p>your profile data </p>
            </div>
        </div>
    </div><!---- End Page Navigation ---->

    <div class="container-fluid">


          <h4 class="text-center">   </h4>
          <div class="row">
            @if (session('status'))
                  <div class="alert alert-success text-center">
                      {{ session('status') }}
                  </div>
              @endif
          </div>
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <div class=" panel panel-default ">
                  <div class="panel-heading">
                    <h2 class="panel-title text-center"> personal data </h2>
                  </div>
                  <form class="form-horizontal" action="/updateProfile" method="post">
                    {{ csrf_field() }}
                    <br /> <br />
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" value="{{ Auth::user()->email }}"
                           id="email" placeholder="Enter email" Disabled >
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="name">Name</label>
                        <div class="col-sm-10">
                          <input type="text" name="name" class="form-control" value="{{ Auth::user()->name }}"
                          id="name" placeholder="Enter Name">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-2" for="address">address</label>
                        <div class="col-sm-10">
                          <input type="text" name="address" class="form-control" value="{{ Auth::user()->address }}"
                          id="address" placeholder="Enter address">
                        </div>
                      </div>


                      <div class="form-group">
                        <label class="control-label col-sm-2" for="phone">phone</label>
                        <div class="col-sm-10">
                          <input type="text" name="phone" class="form-control" value="{{ Auth::user()->phone }}"
                          id="phone" placeholder="Enter phone">
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-default">Update</button>
                        </div>
                      </div>
                    </form>



                </div>
            </div>
          </div>





    </div>








@stop
