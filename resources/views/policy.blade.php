
@extends('layouts.hk_layout')

@section('content')
<?php
use App\Businessinfo;
$info = Businessinfo::where("id" ,1)->first();

?>

<!-------------------------- Page Navigation --------------------------->
<div class="header-page">
    <div class="container center">
        <div class="row">
            <h1>privacy policy</h1>
            <img class="img-responsive" src="images/title-shape.png" alt="">
            <p>For more safety usage</p>
        </div>
    </div>
</div><!---- End Page Navigation ---->

    <!-------------------------- Privacy --------------------------->
    <div class="privacy-policy">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>{{$info->privacy_title1}}</h3>
                    <p class="gray-p">
                      {{$info->privacy_text1}}
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>{{$info->privacy_title2}} </h3>
                    <p class="gray-p">
                      {{$info->privacy_text2}}
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>{{$info->privacy_title3}} </h3>
                    <p class="gray-p">
                      {{$info->privacy_text3}}
                     </p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>{{$info->privacy_title4}}</h3>
                    <p class="gray-p">
                      {{$info->privacy_text4}}
                   </p>
                </div>
            </div>
        </div>
    </div>
    <!---- End Privacy ----->



@stop
