
@extends('layouts.hk_layout')

@section('content')





    <!-------------------------- Page Navigation --------------------------->
    <div class="header-page">
        <div class="container center">
            <div class="row">
                <h1>Orders</h1>
                <img class="img-responsive" src="eu_assets/images/title-shape.png" alt="">
                <p>All the orders that you sent</p>
            </div>
        </div>
    </div><!---- End Page Navigation ---->
    <div class="container">
      <div class="row">
        @if (session('status'))
              <div class="alert alert-success text-center">
                  {{ session('status') }}
              </div>
          @endif
      </div>
      
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <h4 class="text-center"> My orders  </h4>
          <table class="table table-striped">
                        <thead>
                        <tr>
                          <th># Order id </th>
                          <th>Order</th>
                          <th>Total</th>
                          <th>Delivery date</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($orders as $order)
                          <?php $fullyorder = preg_replace(' ["] ', ' ', $order->fullOrder); ?>
                        <tr>
                          <td> #{{ $order->id }}</td>
                          <td>{{ $fullyorder }}</td>
                          <td>{{ $order->total }} $</td>
                          <td>{{ $order->delivery_date }}</td>
                        </tr>
                           @endforeach
                        </tbody>
                      </table>
        </div>
      </div>
    </div>








@stop
