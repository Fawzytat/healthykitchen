@extends('layouts.dashboard')

@section('styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
@endsection


@section('content')
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->

                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/admin">Dashboard</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Website content</a>
                                    <i class="fa fa-circle"></i>
                                </li>

                            </ul>
                            <div class="page-toolbar">
                              <!-- Placeholder for the action button -->
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Healthy Kitchen
                            <small>Business info</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                    <!-- OUR OWN CONTENT  -->
                    @if (session('status'))
                          <div class="alert alert-success">
                              {{ session('status') }}
                          </div>
                      @endif
                      <div class="row">
                        <div class="col-md-4">
                          <div class="portlet light bordered">
                                  <div class="portlet-title">
                                      <div class="caption font-red-sunglo">
                                          <i class="fa fa-briefcase font-red-sunglo"></i>
                                          <span class="caption-subject bold uppercase"> Business info</span>
                                      </div>

                                  </div>
                                  <div class="portlet-body form">
                                      <form role="form" action="businessUpdate" method="POST">
                                        {{ csrf_field() }}
                                          <div class="form-body">

                                              <div class="form-group form-md-line-input">
                                                  <input type="text" class="form-control" id="form_control_1" value="{{$info->phone}}" placeholder="Enter your Phone number" name="phone">
                                                  <label for="form_control_1">Phone</label>
                                                  <span class="help-block">Only digits</span>
                                              </div>

                                              <div class="form-group form-md-line-input">
                                                  <input type="text" class="form-control" id="form_control_1" value="{{$info->email}}" placeholder="Enter your Email" name="email">
                                                  <label for="form_control_1">Email</label>
                                                  <span class="help-block">make sure you type a valid email</span>
                                              </div>


                                              <div class="form-group form-md-line-input">
                                                  <textarea class="form-control" rows="9"  placeholder="Enter more text" name="kitchen_rules">
                                                    {{$info->kitchen_rules}}
                                                  </textarea>
                                                  <label for="form_control_1">Kitchen rules</label>
                                              </div>

                                              <div class="form-actions noborder">
                                                <button type="submit" class="btn blue pull-right">Update</button>
                                                <button type="button" id="backButton" class="btn default pull-right">Back to homepage</button>
                                              </div>



                                      </form>
                                  </div>
                              </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                          <div class="portlet light bordered">
                                  <div class="portlet-title">
                                      <div class="caption font-red-sunglo">
                                          <i class="fa fa-flag font-red-sunglo"></i>
                                          <span class="caption-subject bold uppercase"> About us</span>
                                      </div>

                                  </div>

                                  <div class="portlet-body form">
                                      <!-- BEGIN FORM-->
                                      <form action="/aboutUpdate" method="post" class="form-horizontal form-bordered">
                                        {{ csrf_field() }}
                                          <div class="form-body">
                                              <div class="form-group">

                                                  <div class="col-md-10">
                                                      <textarea name="aboutus" class="wysihtml5 form-control" rows="8">
                                                        {{$info->about}}
                                                      </textarea>
                                                  </div>
                                              </div>

                                          </div>
                                          <div class="form-actions">
                                              <div class="row">
                                                  <div class="col-md-offset-2 col-md-10">
                                                      <button type="submit" class="btn red">
                                                          <i class="fa fa-check"></i> Submit</button>
                                                      <button type="button" class="btn grey-salsa btn-outline">Cancel</button>
                                                  </div>
                                              </div>
                                          </div>
                                      </form>
                                      <!-- END FORM-->
                                  </div>


                            </div>
                        </div>
                      </div>

                    <!--END OF OUR OWN CONTENT-->

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

                <script type="text/javascript">
                    var back1 = document.getElementById("backButton");
                    //var back2 = document.getElementById("backButton2");
                    back1.addEventListener("click", redirectHome);
                  //  back2.addEventListener("click", redirectHome);

                    function redirectHome() {
                    location.href = "/home";
                  }

                </script>

      @stop

      @section('pageLevelPlugins')
      <!-- BEGIN PAGE LEVEL PLUGINS -->
      <script src="assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
      <script src="assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
      <script src="assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
      <script src="./assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
      <script src="assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
      <!-- END PAGE LEVEL PLUGINS -->
      @endsection

      @section('pageLevelScripts')
      <!-- BEGIN PAGE LEVEL SCRIPTS -->
      <script src="assets/pages/scripts/components-editors.js" type="text/javascript"></script>
      <!-- END PAGE LEVEL SCRIPTS -->
      @endsection
