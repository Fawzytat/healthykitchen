@extends('layouts.dashboard')


@section('styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
     <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
     <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
     <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
@endsection


@section('content')
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->

                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/admin">Dashboard</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Meals</a>
                                    <i class="fa fa-circle"></i>
                                </li>

                            </ul>
                            <div class="page-toolbar">
                              <!-- Placeholder for the action button -->
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Healthy Kitchen
                            <small>Meals</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                    <!-- OUR OWN CONTENT  -->
                    @if (session('status'))
                          <div class="alert alert-success">
                              {{ session('status') }}
                          </div>
                    @endif
                    <div class="row">
                      <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-bell-o"></i>Menu Meals </div>

                                                    <div class="btn-group pull-right">
                                                        <button id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#draggable"> Add New Meal
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>

                                                    <!-- Add meal modal  -->
                                                        <div class="modal fade draggable-modal" id="draggable" tabindex="-1" role="basic" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                        <h4 class="modal-title">Add to your menu</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <!--Form start -->

                                                                      <form action="/meals" method="POST" class="form-horizontal">

                                                                          {{ csrf_field() }}

                                                                          <div class="form-body">

                                                                              <div class="form-group">
                                                                                  <label class="col-md-3 control-label">Meal name</label>
                                                                                  <div class="col-md-9">
                                                                                      <div class="input-icon">
                                                                                          <i class="fa fa-spoon"></i>
                                                                                          <input type="text" name="name" class="form-control" placeholder="EX : FLAFEL PITA"> </div>
                                                                                  </div>
                                                                              </div>

                                                                              <div class="form-group">
                                                                                  <label class="col-md-3 control-label">Meal ID</label>
                                                                                  <div class="col-md-9">
                                                                                      <div class="input-icon">
                                                                                          <i class="fa fa-spoon"></i>
                                                                                          <input type="text" name="mid" class="form-control" placeholder="EX : 14"> </div>
                                                                                  </div>
                                                                              </div>

                                                                              <div class="form-group">
                                                                                <label class="col-md-3 control-label">Category</label>
                                                                                <div class="col-md-9">
                                                                                      <select name="category" class="form-control">
                                                                                          @foreach($categories as $category)
                                                                                          <option value="{{ $category->id }}" >{{ $category->name}}</option>
                                                                                          @endforeach

                                                                                      </select>
                                                                                  </div>
                                                                                </div>
                                                                              <div class="form-group">
                                                                                  <label class="col-md-3 control-label">Description</label>
                                                                                  <div class="col-md-9">
                                                                                      <div class="input-icon">
                                                                                          <i class="fa fa-file"></i>
                                                                                          <input type="text" name="description" class="form-control" placeholder="EX : Salad, Hummus, Tahini, Drink"> </div>
                                                                                  </div>
                                                                              </div>

                                                                              <div class="form-group">
                                                                                  <label class="col-md-3 control-label">Price</label>
                                                                                  <div class="col-md-9">
                                                                                      <div class="input-icon">
                                                                                          <i class="fa fa-dollar"></i>
                                                                                          <input type="text" name="price" class="form-control" placeholder="EX : 9$ "> </div>
                                                                                  </div>
                                                                              </div>



                                                                          </div>
                                                                      <!-- </form> -->
                                                                      <!-- END FORM-->

                                                                        <!-- Form end  -->
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn green">Add to menu</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div>
                                                    <!-- End of add meal modal-->

                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <i class="fa fa-briefcase"></i> title
                                                        </th>
                                                        <th>
                                                            <i class="fa fa-key"></i> ID
                                                        </th>
                                                        <th class="hidden-xs">
                                                            <i class="fa fa-user"></i> category </th>
                                                        <th>
                                                            <i class="fa fa-shopping-cart"></i> price </th>
                                                        <th> description </th>
                                                        <th> Most popular </th>
                                                        <th> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($meals as $meal)
                                                    <tr>
                                                        <td class="highlight">
                                                            <div class="success"></div>
                                                            <a href="javascript:;"> {{ $meal->name }} </a>
                                                        </td>
                                                        <td class="highlight">

                                                            <a> {{ $meal->mid }} </a>
                                                        </td>
                                                        <td class="hidden-xs"> {{ $meal->category->name }} </td>
                                                        <td> {{ $meal->price }} $ </td>
                                                        <td> {{ $meal->ingredients }} </td>
                                                        <td>
                                                           @if($meal->popular == "no")
                                                           <a style="text-decoration: none;">
                                                           <span class="label label-sm label-info addToPopMeal"
                                                           data-toggle="modal" data-id="{{ $meal->id}}" href="#addtopopular">
                                                           Add to popular </span>
                                                          </a>
                                                                <!-- Add to popular-->
                                                                          <!-- /.modal -->
                                                                          <div class="modal fade bs-modal-sm" id="addtopopular" tabindex="-1" role="dialog" aria-hidden="true">
                                                                              <div class="modal-dialog modal-sm">
                                                                                  <div class="modal-content">
                                                                                      <div class="modal-header">
                                                                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                          <h4 class="modal-title">Add to most popular</h4>
                                                                                      </div>
                                                                                      <div class="modal-body">
                                                                                        {!! Form::open([
                                                                                          'method' => 'POST',
                                                                                          'route' => ['makeFeatured'] ,
                                                                                          'class'=>'form-horizontal',
                                                                                          'files'=>true ,
                                                                                         ]) !!}
                                                                                          {{ csrf_field() }}

                                                                                          <input type="hidden" name="addToPopMeal" id="addToPopMeal"/>

                                                                                          <div class="fileinput fileinput-new center" data-provides="fileinput">
                                                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                                                <div>
                                                                                                    <span class="btn default btn-file">
                                                                                                        <span class="fileinput-new"> Select image </span>
                                                                                                        <span class="fileinput-exists"> Change </span>
                                                                                                        <input type="file" name="featuredImage"> </span>
                                                                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                                                </div>
                                                                                            </div>


                                                                                      </div>
                                                                                      <div class="modal-footer">
                                                                                          <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                                                          <button type="submit" class="btn green">Update</button>
                                                                                       {!! Form::close() !!}

                                                                                      </div>
                                                                                  </div>
                                                                                  <!-- /.modal-content -->
                                                                              </div>
                                                                              <!-- /.modal-dialog -->
                                                                          </div>
                                                                          <!-- /.modal -->
                                                                <!-- end of popular add -->
                                                           @else
                                                           <a style="text-decoration: none;">
                                                             <span class="label label-sm label-danger delPopMeal" data-toggle="modal"
                                                             data-id="{{$meal->id}}" href="#removeFromPop"> Remove from popular </span>
                                                           </a>

                                                           <!-- remove from popular-->
                                                                     <!-- /.modal -->
                                                                     <div class="modal fade bs-modal-sm" id="removeFromPop" tabindex="-1" role="dialog" aria-hidden="true">
                                                                         <div class="modal-dialog modal-sm">
                                                                             <div class="modal-content">
                                                                                 <div class="modal-header">
                                                                                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                     <h4 class="modal-title">Remove from most popular</h4>
                                                                                 </div>
                                                                                 <div class="modal-body">
                                                                                   {!! Form::open([
                                                                                     'method' => 'POST',
                                                                                     'route' => ['makeunFeatured'] ,
                                                                                     'class'=>'form-horizontal',
                                                                                    ]) !!}
                                                                                     {{ csrf_field() }}

                                                                                     <input type="hidden" name="remFromPopMeal" id="remFromPopMeal"/>
                                                                                     <h4> Are you sure you want to remove this item ?</h4>

                                                                                 </div>
                                                                                 <div class="modal-footer">
                                                                                     <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                                                     <button type="submit" class="btn green">Remove</button>
                                                                                  {!! Form::close() !!}

                                                                                 </div>
                                                                             </div>
                                                                             <!-- /.modal-content -->
                                                                         </div>
                                                                         <!-- /.modal-dialog -->
                                                                     </div>
                                                                     <!-- /.modal -->
                                                           <!-- end of popular remove -->
                                                           @endif
                                                       </td>
                                                        <td>
                                                          <!-- Edit _________________________________ Process -->
                                                            <button  class="btn btn-outline btn-circle btn-sm purple editmeal"
                                                             data-toggle="modal"
                                                             data-id="{{ $meal->id}}"
                                                             data-name="{{ $meal->name}}"
                                                             data-category="{{ $meal->category->id}}"
                                                             data-mid="{{ $meal->mid}}"
                                                             data-description="{{ $meal->ingredients}}"
                                                             data-price="{{ $meal->price}}"
                                                             href="#editmeal"
                                                              >
                                                                <i class="fa fa-edit"></i> Edit
                                                            </button>

                                                            <!-- edit meal modal  -->
                                                                <div class="modal fade draggable-modal" id="editmeal" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">Edit meal</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <!--Form start -->


                                                                                {!! Form::model($meal, [
                                                                                      'method' => 'PATCH',
                                                                                      'route' => ['meals.update', $meal->id] ,
                                                                                      'class'=>'form-horizontal',
                                                                                  ]) !!}


                                                                                  {{ csrf_field() }}

                                                                                  <div class="form-body">
                                                                                    <input type="hidden" name="editMealId" id="MealtoEdit"/>

                                                                                      <div class="form-group">
                                                                                          <label class="col-md-3 control-label">Meal name</label>
                                                                                          <div class="col-md-9">
                                                                                              <div class="input-icon">
                                                                                                  <i class="fa fa-spoon"></i>
                                                                                                  <input type="text" id="MealtoEditname" name="name" value="{{ $meal->name }}" class="form-control" placeholder="EX : FLAFEL PITA"> </div>
                                                                                          </div>
                                                                                      </div>

                                                                                      <div class="form-group">
                                                                                          <label class="col-md-3 control-label">Meal ID</label>
                                                                                          <div class="col-md-9">
                                                                                              <div class="input-icon">
                                                                                                  <i class="fa fa-key"></i>
                                                                                                  <input type="text" id="MealtoEditmid" name="mid" value="{{ $meal->mid }}" class="form-control" placeholder="EX : 14"> </div>
                                                                                          </div>
                                                                                      </div>

                                                                                      <div class="form-group">
                                                                                        <label class="col-md-3 control-label">Category</label>
                                                                                        <div class="col-md-9">
                                                                                              <select name="category" id="MealtoEditcat" class="form-control">
                                                                                                  @foreach($categories as $category)
                                                                                                  <option value="{{ $category->id }}" >{{ $category->name}}</option>
                                                                                                  @endforeach

                                                                                              </select>
                                                                                          </div>
                                                                                        </div>
                                                                                      <div class="form-group">
                                                                                          <label class="col-md-3 control-label">Description</label>
                                                                                          <div class="col-md-9">
                                                                                              <div class="input-icon">
                                                                                                  <i class="fa fa-file"></i>
                                                                                                  <input type="text" name="description" id="MealtoEditdes" value="{{ $meal->ingredients }}" class="form-control" placeholder="EX : Salad, Hummus, Tahini, Drink"> </div>
                                                                                          </div>
                                                                                      </div>

                                                                                      <div class="form-group">
                                                                                          <label class="col-md-3 control-label">Price</label>
                                                                                          <div class="col-md-9">
                                                                                              <div class="input-icon">
                                                                                                  <i class="fa fa-dollar"></i>
                                                                                                  <input type="text" name="price" id="MealtoEditprice" value="{{ $meal->price }}" class="form-control" placeholder="EX : 9$ "> </div>
                                                                                          </div>
                                                                                      </div>



                                                                                  </div>
                                                                              <!-- </form> -->
                                                                              <!-- END FORM-->

                                                                                <!-- Form end  -->
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                                                <button type="submit" class="btn green">Update</button>
                                                                            </div>
                                                                            {!! Form::close() !!}
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            <!-- End of edit meal modal-->

                                                          <!-- End of ___________________________________edit process-->
                                                              <button class="btn btn-outline btn-circle btn-sm red deletemeal"  data-toggle="modal" data-id="{{ $meal->id }}" href="#areyousure">
                                                                <i class="fa fa-trash"></i>
                                                                Delete
                                                             </button>

                                                             <!-- /.modal -->
                                                             <div class="modal fade bs-modal-sm" id="areyousure" tabindex="-1" role="dialog" aria-hidden="true">
                                                                 <div class="modal-dialog modal-sm">
                                                                     <div class="modal-content">
                                                                         <div class="modal-header">
                                                                             <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                             <h4 class="modal-title">Remove meal</h4>
                                                                         </div>
                                                                         <div class="modal-body">
                                                                           {!! Form::open([
                                                                                'method' => 'DELETE',
                                                                                'route' => ['meals.destroy', $meal->id]
                                                                            ]) !!}
                                                                             {{ csrf_field() }}
                                                                             <h3> Are you sure you want to remove this meal from your menu ? </h3>
                                                                             <input type="hidden" name="deletedMealId" id="MealtoDelete"/>

                                                                         </div>
                                                                         <div class="modal-footer">
                                                                             <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                                             <button type="submit" class="btn green">Remove</button>
                                                                          {!! Form::close() !!}

                                                                         </div>
                                                                     </div>
                                                                     <!-- /.modal-content -->
                                                                 </div>
                                                                 <!-- /.modal-dialog -->
                                                             </div>
                                                             <!-- /.modal -->
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>

                    </div>
                    <!--END OF OUR OWN CONTENT-->

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                <script type="text/javascript">
                    $(document).on("click", ".deletemeal", function () {
                     var delmealid = $(this).data('id');
                     $(".modal-body #MealtoDelete").val(delmealid);
                      });

                    $(document).on("click", ".editmeal", function () {
                     var editmealid = $(this).data('id');
                     var editmealname = $(this).data('name');
                     var editmealcategory = $(this).data('category');
                     console.log(editmealcategory);
                     var editmealmid = $(this).data('mid');
                     var editmealprice = $(this).data('price');
                     var editmealdescription = $(this).data('description');
                     $(".modal-body #MealtoEdit").val(editmealid);
                     $(".modal-body #MealtoEditname").val(editmealname);
                     $(".modal-body #MealtoEditcat").val(editmealcategory);
                     $(".modal-body #MealtoEditmid").val(editmealmid);
                     $(".modal-body #MealtoEditprice").val(editmealprice);
                     $(".modal-body #MealtoEditdes").val(editmealdescription);
                      });

                      $(document).on("click", ".addToPopMeal", function () {
                       var addToPopMeal = $(this).data('id');
                       $(".modal-body #addToPopMeal").val(addToPopMeal);
                        });

                        $(document).on("click", ".delPopMeal", function () {
                         var delPopMeal = $(this).data('id');
                         $(".modal-body #remFromPopMeal").val(delPopMeal);
                          });
                </script>
      @endsection

      @section('pageLevelPlugins')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
         <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
         <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
      @endsection

      @section('pageLevelScripts')
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
      <script src="assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
      <script src="assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
      <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
       <!-- END PAGE LEVEL SCRIPTS -->
      @endsection
