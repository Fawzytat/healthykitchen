@extends('layouts.dashboard')


@section('styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
     <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
     <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
     <link href="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
@endsection


@section('content')
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->

                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/admin">Dashboard</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Meals</a>
                                    <i class="fa fa-circle"></i>
                                </li>

                            </ul>
                            <div class="page-toolbar">
                              <!-- Placeholder for the action button -->
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Healthy Kitchen
                            <small>Meals</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                    <!-- OUR OWN CONTENT  -->
                    @if (session('status'))
                          <div class="alert alert-success">
                              {{ session('status') }}
                          </div>
                    @endif
                    <div class="row">
                      <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-bell-o"></i>Menu Meals </div>

                                                    <div class="btn-group pull-right">
                                                        <button id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#draggablecity"> Add New City
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>

                                                    <!-- Add meal modal  -->
                                                        <div class="modal fade draggable-modal" id="draggablecity" tabindex="-1" role="basic" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                        <h4 class="modal-title">Add to your City list</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <!--Form start -->

                                                                      <form action="/cities" method="POST" class="form-horizontal">

                                                                          {{ csrf_field() }}

                                                                          <div class="form-body">

                                                                              <div class="form-group">
                                                                                  <label class="col-md-3 control-label">City name</label>
                                                                                  <div class="col-md-9">
                                                                                      <div class="input-icon">
                                                                                          <i class="fa fa-spoon"></i>
                                                                                          <input type="text" name="name" class="form-control" placeholder="EX : Manhatten"> </div>
                                                                                  </div>
                                                                              </div>



                                                                          </div>
                                                                      <!-- </form> -->
                                                                      <!-- END FORM-->

                                                                        <!-- Form end  -->
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn green">Add Now</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div>
                                                    <!-- End of add meal modal-->

                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                <thead>
                                                    <tr>
                                                      <th>
                                                        Id
                                                      </th>
                                                        <th>
                                                            <i class="fa fa-briefcase"></i> title
                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($cities as $city)
                                                    <tr>
                                                        <td>
                                                          {{ $city->id }}
                                                        </td>
                                                        <td class="highlight">

                                                            <a href="javascript:;"> {{ $city->city }} </a>
                                                        </td>

                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>

                    </div>
                    <!--END OF OUR OWN CONTENT-->

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

      @endsection

      @section('pageLevelPlugins')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
         <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
         <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
      @endsection

      @section('pageLevelScripts')
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
      <script src="assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
      <script src="assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
      <script src="assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
       <!-- END PAGE LEVEL SCRIPTS -->
      @endsection
