@extends('layouts.dashboard')


@section('styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
     <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
     <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endsection


@section('content')
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->

                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/admin">Dashboard</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Orders</a>
                                    <i class="fa fa-circle"></i>
                                </li>

                            </ul>
                            <div class="page-toolbar">
                              <!-- Placeholder for the action button -->
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Healthy Kitchen
                            <small>Orders</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                    <!-- OUR OWN CONTENT  -->
                    @if (session('status'))
                          <div class="alert alert-success">
                              {{ session('status') }}
                          </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">

                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th> order id </th>
                                                <th> Order </th>
                                                <th> user name </th>
                                                <th> Address </th>
                                                <th> Delivery date </th>
                                                <th> Total </th>
                                                <th> Card details </th>
                                                <th> Notes  </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($orders as $order)
                                          <?php $fullyorder = preg_replace(' ["] ', ' ', $order->fullOrder); ?>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="1" />
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td>
                                                  # {{ $order->id }}
                                                </td>
                                                <td>
                                                      <p>
                                                       {{ nl2br(e($fullyorder)) }}
                                                      </p>
                                                </td>
                                                <td class="center">
                                                <a data-toggle="modal" class="viewUser"
                                                data-email="{{ $order->user->email }}"
                                                data-name="{{ $order->user->name }}"
                                                data-address="{{ $order->user->address }}"
                                                data-phone="{{ $order->user->phone }}"
                                                href="#UserDataModal">
                                                  {{{ isset($order->user->name) ? $order->user->name : '' }}}
                                                </a>

                                                <!-- /.modal -->
                                                <div class="modal fade bs-modal-sm" id="UserDataModal" tabindex="-1" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog modal-sm">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title" id="userTitle"></h4>
                                                            </div>
                                                            <div class="modal-body">

                                                              <div class="panel panel-info">
                                                                      <!-- Default panel contents -->
                                                                      <div class="panel-heading">
                                                                          <h3 class="panel-title"> User data </h3>
                                                                      </div>
                                                                      <div class="panel-body">

                                                                      </div>
                                                                      <!-- Table -->
                                                                      <table class="table">
                                                                          <thead>
                                                                              <tr>
                                                                                  <th width="40%" style="width:40%;" > User name</th>
                                                                                  <th id="userName"></th>

                                                                              </tr>
                                                                          </thead>
                                                                          <tbody>
                                                                              <tr>
                                                                                  <td width="40%"> Phone </td>
                                                                                  <td id="userPhone"></td>

                                                                              </tr>
                                                                              <tr>
                                                                                  <td width="40%"> Address </td>
                                                                                  <td id="userAddress"></td>

                                                                              </tr>
                                                                              <tr>
                                                                                  <td width="40%"> Email </td>
                                                                                  <td id="userEmail" ></td>

                                                                              </tr>
                                                                          </tbody>
                                                                      </table>
                                                                  </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <!-- /.modal -->

                                                </td>
                                                <td class="center">
                                                     {{ $order->address }} in {{ $order->city }}
                                                </td>
                                                <td class="center">
                                                    {{ $order->delivery_date }}
                                                </td>
                                                <td class="center">
                                                    {{ $order->total }} $
                                                </td>
                                                <td class="center">
                                                    <ul class="list-group">
                                                      <li class="list-group-item"> Card No | {{ $order->cred_no }}</li>
                                                      <li class="list-group-item"> Expiry date |{{ $order->exp_date }}</li>
                                                      <li class="list-group-item"> Cvv | {{ $order->cvv }}</li>
                                                    </ul>
                                                </td>
                                                <td class="center">
                                                    {{ $order->notes }}
                                                </td>
                                            </tr>

                                            @endforeach

                                        </tbody>
                                    </table>


                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                    <!--END OF OUR OWN CONTENT-->

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

                <script type="text/javascript">


                    $(document).on("click", ".viewUser", function () {
                     var userphone = $(this).data('phone');
                     var username = $(this).data('name');
                     var useremail = $(this).data('email');
                     var useraddress = $(this).data('address');
                     $(".modal-body #userPhone").html(userphone);
                     $(".modal-body #userName").html(username);
                     $(".modal-header #userTitle").html(username);
                     $(".modal-body #userEmail").html(useremail);
                     $(".modal-body #userAddress").html(useraddress);

                      });

                </script>

      @endsection

      @section('pageLevelPlugins')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
         <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
         <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
      @endsection

      @section('pageLevelScripts')
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
      <script src="assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
      <script src="assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
       <!-- END PAGE LEVEL SCRIPTS -->
      @endsection
