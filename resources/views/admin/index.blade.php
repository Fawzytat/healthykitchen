@extends('layouts.dashboard')

@section('content')
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->

                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/admin">Dashboard</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>

                            </ul>
                            <div class="page-toolbar">
                              <!-- Placeholder for the action button -->
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Healthy Kitchen
                            <small>Dashboard</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->


                        <!-- Start of boxs row -->
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 blue" href="/business">
                                    <div class="visual">
                                        <i class="fa fa-briefcase"></i>
                                    </div>
                                    <div class="details">
                                        <div class="desc"> <h2>Business info</h2> <br /> <h6>Email,phone, about us and rules</h6></div>
                                    </div>

                                </a>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 red" href="/meals">
                                    <div class="visual">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </div>
                                    <div class="details">
                                        <div class="desc">
                                             <h2> Menu </h2>
                                               <br />
                                             <h6>Add and edit meals</h6>
                                       </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 green" href="/cities">
                                    <div class="visual">
                                        <i class="fa fa-shopping-star"></i>
                                    </div>
                                    <div class="details">
                                        <div class="desc">
                                             <h2> Add / remove cities </h2>
                                               <br />

                                       </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <a class="dashboard-stat dashboard-stat-v2 purple" href="/orders">
                                    <div class="visual">
                                        <i class="fa fa-credit-card"></i>
                                    </div>
                                    <div class="details">
                                        <div class="desc">
                                             <h2> Orders </h2>
                                               <br />
                                             <h6> Check and control Orders</h6>
                                       </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-v2 purple" href="/feedbacks">
                                  <div class="visual">
                                      <i class="fa fa-comment"></i>
                                  </div>
                                  <div class="details">
                                      <div class="desc">
                                           <h2> Feedbacks </h2>
                                             <br />
                                           <h6> Check and control Messages </h6>
                                     </div>
                                  </div>
                              </a>
                          </div>

                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <a class="dashboard-stat dashboard-stat-v2 green" href="/policyEdit">
                                  <div class="visual">
                                      <i class="fa fa-shopping-cart"></i>
                                  </div>
                                  <div class="details">
                                      <div class="desc">
                                           <h2> Kitchen info </h2>
                                             <br />
                                           <h6> privacy policy and user agreement </h6>
                                     </div>
                                  </div>
                              </a>
                          </div>

                        </div>

                        <!-- Endo of boxs row -->
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
      @stop
