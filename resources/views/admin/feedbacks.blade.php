@extends('layouts.dashboard')


@section('styles')
<!-- BEGIN PAGE LEVEL PLUGINS -->
     <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
     <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
@endsection


@section('content')
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->

                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/admin">Dashboard</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Inbox</a>
                                    <i class="fa fa-circle"></i>
                                </li>

                            </ul>
                            <div class="page-toolbar">
                              <!-- Placeholder for the action button -->
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Healthy Kitchen
                            <small>Feedbacks</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                    <!-- OUR OWN CONTENT  -->
                    @if (session('status'))
                          <div class="alert alert-success">
                              {{ session('status') }}
                          </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">

                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>
                                            <div class="col-md-6">

                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th> Name </th>
                                                <th> Email </th>
                                                <th> Subject </th>
                                                <th> Phone </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($feedbacks as $feedback)
                                            <tr class="odd gradeX">
                                                <td>
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="1" />
                                                        <span></span>
                                                    </label>
                                                </td>
                                                <td> {{ $feedback->name }} </td>
                                                <td>
                                                    <a href=""> {{ $feedback->mail }}</a>
                                                </td>
                                                <td>
                                                     {{ $feedback->subject }}
                                                </td>
                                                <td class="center">{{ $feedback->phone }}</td>
                                                <td>
                                                  <a data-toggle="modal" href="#small"
                                                     class="btn dark btn-sm btn-outline sbold uppercase"
                                                     data-subject="{{ $feedback->subject }}"
                                                     data-message="{{ $feedback->message }}"
                                                    >
                                                    <i class="fa fa-share"></i> View Message </a>
                                                </td>
                                            </tr>

                                            <div class="modal fade bs-modal-sm" id="small" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title" id="FeedbackSubject"></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                          <h4 id="FeedbackMessage"></h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            @endforeach
                                        </tbody>
                                    </table>


                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                    <!--END OF OUR OWN CONTENT-->

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
                <script type="text/javascript">
                                  $(document).on("click", ".btn-outline", function () {
                                   var feedSubject = $(this).data('subject');
                                   $(".modal-header #FeedbackSubject").html(feedSubject);
                                   var feedMessage = $(this).data('message');
                                   $(".modal-body #FeedbackMessage").html(feedMessage);
                  });

                </script>
      @endsection

      @section('pageLevelPlugins')
        <!-- BEGIN PAGE LEVEL PLUGINS -->
         <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
         <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
      @endsection

      @section('pageLevelScripts')
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
      <script src="assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
      <script src="assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
       <!-- END PAGE LEVEL SCRIPTS -->
      @endsection
