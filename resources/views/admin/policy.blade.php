@extends('layouts.dashboard')

@section('content')
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->

                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="/admin">Dashboard</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Website content</a>
                                    <i class="fa fa-circle"></i>
                                </li>

                            </ul>
                            <div class="page-toolbar">
                              <!-- Placeholder for the action button -->
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> Healthy Kitchen
                            <small>Kitchen info</small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                    <!-- OUR OWN CONTENT  -->
                    @if (session('status'))
                          <div class="alert alert-success">
                              {{ session('status') }}
                          </div>
                      @endif
                      <div class="row">

                        <div class="col-md-6">
                          <div class="portlet light bordered">
                                  <div class="portlet-title">
                                      <div class="caption font-red-sunglo">
                                          <i class="fa fa-flag font-red-sunglo"></i>
                                          <span class="caption-subject bold uppercase"> TERMS & CONDITIONS</span>
                                      </div>

                                  </div>
                                  <div class="portlet-body form">
                                      <form role="form" action="/updateTerms" method="post">
                                        {{ csrf_field() }}
                                          <div class="form-body">

                                              <div class="form-group form-md-line-input">
                                                  <textarea name="ourterms" class="form-control" rows="3" placeholder="will be show in home page">
                                                    {{$info->terms}}
                                                  </textarea>
                                                  <label for="form_control_1">TERMS & CONDITIONS </label>
                                              </div>

                                              <div class="form-actions noborder">
                                                <button type="submit" class="btn blue pull-right">update</button>
                                                <button id="backButton" type="button" class="btn default pull-right">Back to homepage</button>
                                              </div>

                                      </form>
                                  </div>
                              </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                          <div class="portlet light bordered">
                                  <div class="portlet-title">
                                      <div class="caption font-red-sunglo">
                                          <i class="fa fa-flag font-red-sunglo"></i>
                                          <span class="caption-subject bold uppercase"> PRIVACY POLICY</span>
                                      </div>

                                  </div>
                                  <div class="portlet-body form">
                                      <form role="form" action="/updatePolicy" method="post">
                                        {{ csrf_field() }}
                                          <div class="form-body">


                                              <div class="form-group form-md-line-input">
                                                  <input type="text" class="form-control" id="form_control_1" value="{{$info->privacy_title1}}"
                                                   placeholder="Enter Section 1 title" name="privacy_title1">
                                                  <label for="form_control_1"> Section 1 title</label>

                                              </div>


                                              <div class="form-group form-md-line-input">
                                                  <textarea name="privacy_text1" class="form-control" rows="6" placeholder="Section 1 text">
                                                    {{$info->privacy_text1}}
                                                  </textarea>
                                                  <label for="form_control_1"> Section 1 text </label>
                                              </div>

                                              <div class="form-group form-md-line-input">
                                                  <input type="text" class="form-control" id="form_control_1" value="{{$info->privacy_title2}}"
                                                   placeholder="Enter Section 2 title" name="privacy_title2">
                                                  <label for="form_control_1"> Section 2 title</label>

                                              </div>


                                              <div class="form-group form-md-line-input">
                                                  <textarea name="privacy_text2" class="form-control" rows="6" placeholder="Section 2 text">
                                                    {{$info->privacy_text2}}
                                                  </textarea>
                                                  <label for="form_control_1"> Section 2 text </label>
                                              </div>


                                              <div class="form-group form-md-line-input">
                                                  <input type="text" class="form-control" id="form_control_1" value="{{$info->privacy_title3}}"
                                                   placeholder="Enter Section 3 title" name="privacy_title3">
                                                  <label for="form_control_1"> Section 3 title</label>

                                              </div>


                                              <div class="form-group form-md-line-input">
                                                  <textarea name="privacy_text3" class="form-control" rows="6" placeholder="Section 3 text">
                                                    {{$info->privacy_text3}}
                                                  </textarea>
                                                  <label for="form_control_1"> Section 3 text </label>
                                              </div>


                                              <div class="form-group form-md-line-input">
                                                  <input type="text" class="form-control" id="form_control_1" value="{{$info->privacy_title4}}"
                                                   placeholder="Enter Section 4 title" name="privacy_title4">
                                                  <label for="form_control_1"> Section 4 title</label>

                                              </div>


                                              <div class="form-group form-md-line-input">
                                                  <textarea name="privacy_text4" class="form-control" rows="6" placeholder="Section 4 text">
                                                    {{$info->privacy_text4}}
                                                  </textarea>
                                                  <label for="form_control_1"> Section 4 text </label>
                                              </div>






                                              <div class="form-actions noborder">
                                                <button type="submit" class="btn blue pull-right">update</button>
                                                <button id="backButton2" type="button" class="btn default pull-right">Back to homepage</button>
                                              </div>

                                      </form>
                                  </div>
                              </div>
                            </div>
                        </div>
                      </div>

                    <!--END OF OUR OWN CONTENT-->

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

                <script type="text/javascript">
                    var back1 = document.getElementById("backButton");
                    var back2 = document.getElementById("backButton2");
                    back1.addEventListener("click", redirectHome);
                    back2.addEventListener("click", redirectHome);

                    function redirectHome() {
                    location.href = "/home";
                  }

                </script>
      @stop
