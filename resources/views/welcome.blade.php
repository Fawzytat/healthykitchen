@extends('layouts.hk_layout')


<?php
use App\Businessinfo;
$info = Businessinfo::where("id" ,1)->first();

?>
@section('content')
    <!-- Eat Healthy ------------------------ -->

    <div class="content-wrap">
        <div class="container-fullwidth clearfix slider-image">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
                    <div class="eat-healthy">
                        <img class="img-responsive" src="eu_assets/images/slider-badge.png" alt="">
                        <div class="center">
                            <a href="#" class="button button-mini" data-scrollto=".menu" data-offset="0">Place an
                                order</a>
                        </div>
                        <a href="" data-scrollto=".items" data-offset="0"><img class="img-responsive arrow"
                                                                               src="eu_assets/images/arrowbottom.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Eat Healthy ---->


    <!-- Catering Mobile ------------------------- -->
    <div class="catering hidden-sm hidden-md hidden-lg">
        <div class="container center">
            <div class="row">
                <div class="col-xs-4">
                    <div class="why-us">
                        <img class="img-responsive" src="eu_assets/images/icon-1.png" alt="">
                        <p>Fresh & Delicious</p>
                    </div>
                </div>

                <div class="col-xs-4">
                    <div class="why-us">
                        <img class="img-responsive" src="eu_assets/images/icon-2.png" alt="">
                        <p>Fast Delivery</p>
                    </div>
                </div>

                <div class="col-xs-4">
                    <div class="why-us">
                        <img class="img-responsive" src="eu_assets/images/icon-3.png" alt="">
                        <p>Event Catering</p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Catering Mobile ----->


    <!-- Items Slider ------------------------- -->
    <div class="items">
        <div class="container center">
            <div class="row">
                <h1>most popular items</h1>
                <img class="img-responsive" src="eu_assets/images/title-shape.png" alt="">
                <p>fresh . fast . tasty</p>

                <div id="oc-testi" class="owl-carousel testimonials-carousel carousel-widget" data-margin="20"
                     data-items-xs="1" data-items-sm="2" data-items-lg="3">
                     @foreach($foods as $food)
                       @if($food->popular == "yes")
                      <div class="oc-item">
                          <div class="testimonial">
                              <div class="item-data">
                                  <div class="data">
                                      <h3>{{ $food->name }}</h3>
                                      <span>{{ $food->price }} $</span>
                                      <p class="kind">{{ $food->category->name}}</p>
                                      <p class="content">{{ $food->ingredients }}</p>
                                      <div class="center">


                                            <button href="#" id="{{$food->id}}" class="button button-mini addItem ">ADD TO CART</button>


                                          <h6 class="hidden text-success">
                                            @if(Auth::user())
                                            Added successfuly !
                                            @else
                                            Please login/register first !
                                            @endif
                                          </h6>
                                      </div>
                                  </div>
                              </div>
                              <div class="item-img">
                                  <img class="img-responsive" src="{{asset('eu_assets/images/pop_meals/').'/'.$food->picture}}" alt="">
                              </div>
                          </div>
                      </div>
                      @endif
                    @endforeach


                </div>
            </div>
        </div>
    </div><!-- End Item Slider ----->


    <!--- Start Menu ------------------------- -->
    <div class="menu" name="menusec" id="menusec">
        <div class="content-wrap">
            <div class="container-fullwidth clearfix">
                <div class="row">
                    <div class="center">
                        <h1>our menu</h1>
                        <img class="img-responsive" src="eu_assets/images/title-shape-2.png" alt="">
                        <p>all you need</p>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="tabs clearfix menu-tabs" id="tab-1">

                                <ul class="tab-nav clearfix">

                                    @foreach($categories as $category)
                                    <li><a href="#tabs-{{ $category->id }}">{{ $category->name }}</a></li>
                                    @endforeach
                                </ul>


                                <!-- <div class="col-md-12">
                                    <select class="select-nav">

                                        @foreach($categories as $category)
                                        <option>
                                        <a href="#tabs-{{ $category->id }}">{{ $category->name }}</a>
                                        </option>
                                        @endforeach
                                    </select>
                                </div> -->

                                <div class="tab-container">
                                  @foreach($categories as $category)

                                    <div class="tab-content clearfix" id="tabs-{{ $category->id }}">
                                        <div class="row">
                                          <?php $meals = $category->meals;
                                                $meals = $meals->sortBy(function($meal){
                                                return $meal->mid;
                                            });
                                          ?>
                                            @foreach($meals as $meal)
                                            <div class="col-xs-12 co-sm-3 col-md-3 col-lg-3">
                                                <div class="menu-item">
                                                    <div class="header">
                                                        <div class="item-number">
                                                            <p>#{{$meal->mid}}</p>
                                                        </div>
                                                        <div>
                                                            <div class="item-name">
                                                                <p>{{$meal->name}}</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="body">
                                                        <span>${{$meal->price}}</span>
                                                        <p>{{$meal->ingredients}} </p>
                                                    </div>

                                                    <div class="center">
                                                        <button href="#" id="{{$meal->id}}" class="button button-mini addItem ">ADD TO CART</button>
                                                        <h6 class="hidden text-success">
                                                          @if(Auth::user())
                                                          Added successfuly !
                                                          @else
                                                          Please login/register first !
                                                          @endif
                                                        </h6>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach



                                        </div>
                                    </div>

                                      @endforeach


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!---- End Menu --- -->


    <!------------------------- Start About ------------------------- -->
    <div class="about">
        <div class="content-wrap">
            <div class="container-fullwidth clearfix">
                <div class="row">
                    <div class="col-sx-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="about-img">
                            <img src="eu_assets/images/about-image.png" alt="">
                        </div>
                    </div>

                    <div class="col-sx-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="about-text center">
                            <h1>healthy kitchen</h1>
                            <img class="img-responsive" src="eu_assets/images/title-shape.png" alt="">
                            <p>perfect catered affairs</p>

                              <h3>
                              <?php

                                echo $info->about;

                                ?>
                          </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End About ----->


    <!-- Start Contact Us ------------------------- -->
    <div class="contact-us" id="contactsec">
        <div class="container center">
            <h1>your feedback matters</h1>
            <img class="img-responsive" src="eu_assets/images/title-shape.png" alt="">
            <p>send us a message</p>



            <div class="contact-form">

                <form action="/feedbacks" method="POST">
                  {{ csrf_field() }}
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <input type="text" class="sm-form-control daterange1" placeholder="Name" name="name" required>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <input type="email" class="sm-form-control daterange1" placeholder="Email" name="email" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <input type="number" class="sm-form-control daterange1" placeholder="Phone" name="phone">
                        </div>

                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            <input type="text" class="sm-form-control daterange1" placeholder="Subject" name="subject">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <textarea class="sm-form-control" placeholder="Message" name="message" required></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <button type="submit" class="button button-mini">send message</button>
                        <p>We'll do our best to get back to you within 6-8 working hours</p>
                    </div>
                </form>
            </div>
        </div>
    </div><!---- End Contact Us ----->
    @stop
