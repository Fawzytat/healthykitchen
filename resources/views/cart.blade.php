
@extends('layouts.hk_layout')

@section('content')





    <!-------------------------- Page Navigation --------------------------->
    <div class="header-page">
        <div class="container center">
            <div class="row">
                <h1>cart</h1>
                <img class="img-responsive" src="eu_assets/images/title-shape.png" alt="">
                <p>All the stuff that you choosed</p>
            </div>
        </div>
    </div><!---- End Page Navigation ---->


    <!-------------------------- Cart --------------------------->
    <div class="cart-page">
        <div class="container center">
            <div class="row">
                <div class="table-responsive">
                    <table class="table">
                        <thead>

                        <tr>
                            <th width="5%">&nbsp; &nbsp;</th>
                            <th width="20%">Meal Name</th>
                            <th width="15%" class="text-center">Meal Price</th>
                            <th width="15%" class="text-center">Quantity</th>
                            <th width="15%" class="text-center">Total</th>
                        </tr>
                        </thead>
                        <tbody>

                          @foreach($items as $row)

                        <tr>
                            <td><a data-deleteBtn="" href="#"><i class="icon-trash2"></i></a></td>
                            <td class="text-left">{{ $row->name }}</td>
                            <td>${{ $row->price }}</td>
                            <td>
                                <div class="cart-quantity">
                                    <button class="quantity-btn minuus">-</button>
                                    <input price="{{ $row->price }}" class="text-center" id="{{ $row->rowId }}" type="text" value="{{ $row->qty }}" />
                                    <button class="quantity-btn pluss">+</button>
                                </div>
                            </td>
                            <td id="ItemTotal">{{ ($row->price)*($row->qty) }}</td>
                        </tr>

                          @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row to-checkout">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                    <div class="row">
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                            <h3 class="text-left">Cart Subtotal</h3>
                        </div>
                        <div class="col-xs-6 col-sm-8 col-md-6 col-lg-6">
                            <h3 class="text-left" id="alltotal">{{ Cart::subtotal() }}</h3>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      @if(Cart::count() == 0 )
                        <a href="/" class="button button-mini">back to menu</a>
                      @else
                    <a href="/checkout" type="submit" class="button button-mini">proceed to checkout</a>
                      @endif
                </div>
            </div>
        </div>
    </div><!---- End Cart ----->

    <script>

    </script>

  @stop
