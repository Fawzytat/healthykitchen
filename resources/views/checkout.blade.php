<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="author" content="SemiColonWeb"/>

    <link rel="icon" href="eu_assets/images/favicon.png" type="image/x-icon"/>

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/bootstrap.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/style.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/dark.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/font-icons.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/animate.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/magnific-popup.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/healthy.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/responsive.css" type="text/css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <!-- Date & Time Picker CSS -->
    <link rel="stylesheet" href="eu_assets/demos/travel/css/datepicker.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/components/timepicker.css" type="text/css"/>
    <link rel="stylesheet" href="eu_assets/css/components/daterangepicker.css" type="text/css"/>

    <!-- Document Title
    ============================================= -->
    <title>Healthy Kitchen - Home</title>

</head>

<body class="stretched">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">


  @include('includes.header')


    <!--         Page Navigation --------------------------->
    <div class="header-page">
        <div class="container center">
            <div class="row">
                <h1>cart</h1>
                <img class="img-responsive" src="eu_assets/images/title-shape.png" alt="">
                <p>All the stuff that you choosed</p>
            </div>
        </div>
    </div>
    <!--    End Page Navigation ---->

        <!-- Check Out --------------------------->
        <div class="container">
          <br />
          <div class="row">
            @if ($errors->any())
                  <div class="alert alert-danger text-center">
                      <ul style="list-style: none;">
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
          </div>
        </div>


        <form method="post"  id="checkoutForm">
          {{ csrf_field() }}
        <div class="checkout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="user-details">
                            <h3>Hello, {{ Auth::user()->name }}</h3>
                            <p class="gray-p">Please specify the address you want to deliver the order to and any other
                                extra details</p>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <input name="address" type="text" value="{{ Auth::user()->address }}" class="sm-form-control" placeholder="Address" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <input name="city" type="text" class="sm-form-control" placeholder="City" required>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <h3>Order to be recieved at</h3>
                                <input name="delivery_date" style="height: 52px;" type="text" class="tleft sm-form-control datetimepicker" required placeholder="MM/DD/YYYY 00:00 AM/PM">
                                </div>


                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <textarea name="notes" class="sm-form-control" placeholder="Notes"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="user-details">
                            <h3>Billing Details</h3>
                            <p class="gray-p">We accept Visa, Master Card and American Express</p>
                            <div class="img-center">
                                <img class="img-responsive" src="eu_assets/images/visacard.png" alt="">
                                <img class="img-responsive" src="eu_assets/images/mastercard.png" alt="">
                                <img class="img-responsive" src="eu_assets/images/americanexpress.png" alt="">
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 " id="checkout-cred_no">
                                    <input id="cred_no" name="cred_no" type="text" class="sm-form-control" placeholder="Credit Card No." required>

                                    <span class="help-block"><strong id="checkout-errors-cred_no"></strong></span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="checkout-exp_mon">
                                    <input id="exp_mon" name="exp_mon" type="text" class="sm-form-control" placeholder="Month" required>

                                    <span class="help-block"><strong id="checkout-errors-exp_mon"></strong></span>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" id="checkout-exp_year">
                                    <input id="exp_year" name="exp_year" type="text" class="sm-form-control" placeholder="Year" required>

                                    <span class="help-block"><strong id="checkout-errors-exp_year"></strong></span>
                                </div>

                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="checkout-cvv">
                                    <input id="cvv" name="cvv" type="text" class="sm-form-control" placeholder="CVV" required>

                                    <span class="help-block"><strong id="checkout-errors-cvv"></strong></span>
                                </div>
                            </div>

                            <p class="privacy-a gray-p">View our <a href="#" data-toggle="modal" data-target="#myModal">Privacy Policy</a></p>

                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                              <div class="modal-dialog">
                                <div class="modal-body">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" id="myModalLabel">Privacy policy</h4>
                                    </div>
                                    <div class="modal-body">
                                      <?php
                                      use App\Businessinfo;
                                      $info = Businessinfo::where("id" ,1)->first();

                                      ?>

                                        <div class="row container-fluid">
                                          <h3> {{ $info->privacy_title1 }} </h3>
                                          <p class="gray-p">
                                             {{ $info->privacy_text1 }}
                                          </p>
                                        </div>
                                        <div class="row container-fluid">
                                          <h3> {{ $info->privacy_title2 }}  </h3>
                                          <p class="gray-p">
                                            {{ $info->privacy_text2 }}
                                          </p>
                                        </div>
                                        <div class="row container-fluid">
                                          <h3> {{ $info->privacy_title3 }}  </h3>
                                          <p class="gray-p">
                                            {{ $info->privacy_text3 }}
                                           </p>
                                        </div>
                                        <div class="row container-fluid">
                                          <h3> {{ $info->privacy_title4 }} </h3>
                                          <p class="gray-p">
                                            {{ $info->privacy_text4 }}
                                          </p>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="your-orders">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <h1>Your Orders</h1>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th width="60%">Product Name</th>
                                        <th width="20%" class="text-center">Quantity</th>
                                        <th width="20%" class="text-center">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($items as $row)
                                    <tr>
                                        <td class="text-left">{{ $row->name }}</td>
                                        <td class="text-center">{{ $row->qty }}</td>
                                        <td class="text-center">{{ ($row->price)*($row->qty) }}</td>
                                    </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 total">
                            <h1>Total <span>${{ intval(Cart::subtotal()) }}</span></h1>
                            <p class="gray-p">Price does not include taxes and delivery charges</p>

                            <button type="submit" class="button button-mini">place order</button>
                            <div class="row container-fluid hidden" id="OrderConfirm">
                              <div class="alert alert-success alert-dismissable text-center">
                                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                  <strong>Thank you!</strong> Your order was sent successfully , you will be redirected to the menu in seconds.
                                </div>
                            </div>

                            <h3 class="uppercase thanks">thank you for choosing healthy kitchen mediterranean</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>

        <!-- End Check Out ----->



            <!--  Start Footer -->
            <div class="footer">
                <div class="container center">
                    <div class="row">
                        <p>We accept all major credit cards</p>
                        <div class="img-center">
                            <img class="img-responsive" src="eu_assets/images/visacard.png" alt="">
                            <img class="img-responsive" src="eu_assets/images/mastercard.png" alt="">
                            <img class="img-responsive" src="eu_assets/images/americanexpress.png" alt="">
                        </div>

                        <div class="terms">
                            <a href="/terms">Terms & Conditions</a>
                            /
                            <a href="/privacypolicy">Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </div><!---- End Footer ----->


            <!--  Start CopyRights -->
            <div class="copyrights">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            <p>&copy 2017 - healthy kitchen all rights reserved. designed and developed by <a
                                    href="http://road9media.com/">road9 media</a></p>
                        </div>

                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 security">
                            <img class="img-responsive" src="eu_assets/images/comodo.png" alt="">
                        </div>
                    </div>
                </div>
            </div><!-- End CopyRights ----->

        </div><!-- #wrapper end -->


        <!-- Go To Top
        ============================================= -->
        <div id="gotoTop" class="icon-angle-up"></div>
          <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/checkout.js"></script>
        <!-- External JavaScripts
        ============================================= -->
        <script type="text/javascript" src="eu_assets/js/jquery.js"></script>
        <script type="text/javascript" src="eu_assets/js/plugins.js"></script>

        <!-- Date & Time Picker JS -->
        <script type="text/javascript" src="eu_assets/js/components/moment.js"></script>
        <script type="text/javascript" src="eu_assets/demos/travel/js/datepicker.js"></script>
        <script type="text/javascript" src="eu_assets/js/components/timepicker.js"></script>

        <!-- Include Date Range Picker -->
        <script type="text/javascript" src="eu_assets/js/components/daterangepicker.js"></script>

        <!-- Footer Scripts
        ============================================= -->
        <script type="text/javascript" src="eu_assets/js/functions.js"></script>
        <script type="text/javascript">
            $(function () {
                $('.travel-date-group .default').datepicker({
                    autoclose: true,
                    startDate: "today",
                });

                $('.travel-date-group .today').datepicker({
                    autoclose: true,
                    startDate: "today",
                    todayHighlight: true
                });

                $('.travel-date-group .past-enabled').datepicker({
                    autoclose: true,
                });
                $('.travel-date-group .format').datepicker({
                    autoclose: true,
                    format: "dd-mm-yyyy",
                });

                $('.travel-date-group .autoclose').datepicker();

                $('.travel-date-group .disabled-week').datepicker({
                    autoclose: true,
                    daysOfWeekDisabled: "0"
                });

                $('.travel-date-group .highlighted-week').datepicker({
                    autoclose: true,
                    daysOfWeekHighlighted: "0"
                });

                $('.travel-date-group .mnth').datepicker({
                    autoclose: true,
                    minViewMode: 1,
                    format: "mm/yy"
                });

                $('.travel-date-group .multidate').datepicker({
                    multidate: true,
                    multidateSeparator: " , "
                });

                $('.travel-date-group .input-daterange').datepicker({
                    autoclose: true
                });

                $('.travel-date-group .inline-calendar').datepicker();

                $('.datetimepicker').datetimepicker({
                    showClose: true
                });

                $('.datetimepicker1').datetimepicker({
                    format: 'LT',
                    showClose: true
                });

                $('.datetimepicker2').datetimepicker({
                    inline: true,
                    sideBySide: true
                });

            });

            $(function () {
                // .daterange1
                $(".daterange1").daterangepicker({
                    "buttonClasses": "button button-rounded button-mini nomargin",
                    "applyClass": "button-color",
                    "cancelClass": "button-light"
                });

                // .daterange2
                $(".daterange2").daterangepicker({
                    "opens": "center",
                    timePicker: true,
                    timePickerIncrement: 30,
                    locale: {
                        format: 'MM/DD/YYYY h:mm A'
                    },
                    "buttonClasses": "button button-rounded button-mini nomargin",
                    "applyClass": "button-color",
                    "cancelClass": "button-light"
                });

                // .daterange3
                $(".daterange3").daterangepicker({
                            singleDatePicker: true,
                            showDropdowns: true
                        },
                        function (start, end, label) {
                            var years = moment().diff(start, 'years');
                            alert("You are " + years + " years old.");
                        });

                // reportrange
                function cb(start, end) {
                    $(".reportrange span").html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }

                cb(moment().subtract(29, 'days'), moment());

                $(".reportrange").daterangepicker({
                    "buttonClasses": "button button-rounded button-mini nomargin",
                    "applyClass": "button-color",
                    "cancelClass": "button-light",
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, cb);

                // .daterange4
                $(".daterange4").daterangepicker({
                    autoUpdateInput: false,
                    locale: {
                        cancelLabel: 'Clear'
                    },
                    "buttonClasses": "button button-rounded button-mini nomargin",
                    "applyClass": "button-color",
                    "cancelClass": "button-light"
                });

                $(".daterange4").on('apply.daterangepicker', function (ev, picker) {
                    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                });

                $(".daterange4").on('cancel.daterangepicker', function (ev, picker) {
                    $(this).val('');
                });

            });

        </script>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-25127970-25', 'auto');
          ga('send', 'pageview');

        </script>
        </body>
        </html>
