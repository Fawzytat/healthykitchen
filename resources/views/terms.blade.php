
@extends('layouts.hk_layout')

@section('content')



    <!-------------------------- Page Navigation --------------------------->
    <div class="header-page">
        <div class="container center">
            <div class="row">
                <h1>Terms and conditions </h1>
                <img class="img-responsive" src="eu_assets/images/title-shape.png" alt="">
                <p> Our Terms and conditions </p>
            </div>
        </div>
    </div><!---- End Page Navigation ---->




    <!-------------------------- Privacy --------------------------->
    <div class="privacy-policy">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>What information do we collect?</h3>
                    <p class="gray-p">We collect information from you when you register on our site, respond to a
                        survey,
                        fill out a form
                        or send kavitas through email.
                        When ordering or registering on our site, as appropriate, you may be asked to enter your: name,
                        e-mail address, mailing address or Type of Kavita. You may, however, visit our site anonymously.
                        Google, as a third party vendor, uses cookies to serve ads on your site. Google's use of the
                        DART
                        cookie enables it to serve ads to your users based on their visit to your sites and other sites
                        on
                        the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and
                        content
                        network privacy policy..</p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>What do we use your information for? </h3>
                    <p class="gray-p">Any of the information we collect from you may be used in one of the following
                        ways:
                        To improve our website
                        (we continually strive to improve our website offerings based on the information and feedback we
                        receive from you)
                        To administer a contest, promotion, survey or other site feature</p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>How do we protect your information? </h3>
                    <p class="gray-p">We implement a variety of security measures to maintain the safety of your
                        personal
                        information when you enter, submit, or access your personal information. </p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h3>Do we use cookies?</h3>
                    <p class="gray-p">Yes (Cookies are small files that a site or its service provider transfers
                        to your
                        computers hard drive through your Web browser (if you allow) that enables the sites or
                        service
                        providers systems to recognize your browser and capture and remember certain information
                        We use cookies to understand and save your preferences for future visits and keep track
                        of
                        advertisements and .
                        Do we disclose any information to outside parties?
                        We do not sell, trade, or otherwise transfer to outside parties your personally
                        identifiable
                        information. This does not include trusted third parties who assist us in operating our
                        website,
                        conducting our business, or servicing you, so long as those parties agree to keep this
                        information
                        confidential. We may also release your information when we believe release is
                        appropriate to comply
                        with the law, enforce our site policies, or protect ours or others rights, property, or
                        safety. </p>
                </div>
            </div>
        </div>
    </div>
    <!---- End Privacy ----->



@stop
